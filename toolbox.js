
var path = require('path');
var fs = require('fs');

module.exports = function(){

  return {
  
    sendEmail: function( message , done )
    {
      var email = require("emailjs/email");

      var server = email.server.connect({
         user: process.env.EMAIL_USER,
         password: process.env.EMAIL_PASSOWRD, 
         host: process.env.EMAIL_HOST,
         port: process.env.EMAIL_PORT,
         ssl: process.env.EMAIL_SSL,
         tls: process.env.EMAIL_TLS,
         timeout: 10 * 1000
      });

      message.attachment = [];
       
      if( typeof message.subject != 'string' || !message.subject)
        message.subject = '';

      if( typeof message.text != 'string' || !message.text )
        message.text = '';

      if( typeof done != 'function' )
        done = function(err, message) { console.log(err || message); };

      if( message.html )
      {
        message.attachment.push({data: message.html, alternative:true, type: 'text/html'});
        delete message.html;
      }
      
      if( !process.env.EMAIL_SEND_ENABLED )
      {
        console.log('skipping actual sending of e-mail in development environment');
        console.log('to: ' + message.to + ' | from: ' + message.from + ' | subject: ' + message.subject);
        done( null , {});
        return true;
      }
   
      // send the message and get a callback with an error or details of the message that was sent 
      server.send(message, done);

      return true;
    },

    renderHtml: function( template_path , data )
    {
      data = typeof data == 'object' ? data : {};
      var template = fs.readFileSync( template_path + '.html', 'utf8');

      Object.keys( data ).forEach(function( prop ){
        var val = data[ prop ];
        var token = '!{'+prop+'}';
        var re = new RegExp( token , "g" );
        template = template.replace( re , val );
      });

      return template;
    },


    downloadFile: function( url , download_path , done )
    {
      var fs = require('fs');
      var os = require('os');
      var path = require('path');
      var request = require('request');
      var uniqid = require('uniqid');
      var md5 = require('MD5');  

      if(!download_path)
      {
        download_path = path.join( os.tmpdir() , md5( uniqid() + String(Date.now()) ) );

        // maintain file extension if possible
        var filename_parts = url.split('/').pop().split('.');

        if( filename_parts.length > 1 )
          download_path = download_path + '.' + filename_parts.pop();
      }

      var file = fs.createWriteStream( download_path );

      request.get( url ).on('response' , function ( response ) {
        if(response.statusCode != 200)
          return false;

        response.pipe( file , { end: false });

        response.on('end', function() {
          file.end();

          if( typeof done == 'function' )
            done( url , download_path ); 
        });
      });
    },

    formatPlural: function( count , single , multiple )
    {
      var str = ( count == 1 ? single : multiple)
      return str.replace('@count', count);
    },

    // http://stackoverflow.com/questions/13627308/add-st-nd-rd-and-th-ordinal-suffix-to-a-number
    ordinalSuffix: function( i ) 
    {
      var j = i % 10,
        k = i % 100;
      if (j == 1 && k != 11)
        return i + "st";
      
      if (j == 2 && k != 12)
        return i + "nd";
      
      if (j == 3 && k != 13)
        return i + "rd";
      
      return i + "th";
    }
  };
}


