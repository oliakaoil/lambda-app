
var path = require('path');
var fs = require('node-fs-extra');
var os = require('os');
var prompt = require('prompt');
var util = require('util');
var wrench = require('wrench');
var archiver = require('archiver');

var dist_path = path.join( __dirname , 'dist' );

if(!fs.existsSync( dist_path ))
{
  console.log('could not read distribution path: ' + dist_path);
  process.exit();
}

var task_basepath = path.join( __dirname , 'tasks' );
var tasks = fs.readdirSync( task_basepath );

if(!fs.existsSync( task_basepath ))
{
  console.log('could not read task path: ' + task_basepath);
  process.exit();
}

if(!tasks.length)
{
  console.log('no tasks found');
  process.exit();
}

if( process.argv.length > 2 )
{
  var task = process.argv[2] + '.js';

  if( tasks.indexOf( task ) != -1 )
    tasks = [ task ];
}

var environment = ( process.argv.length > 3 ? process.argv[3] : 'local' );

switch( environment )
{
  case '--prod': environment = 'live'; break;
  case '--dev': environment = 'dev'; break;
  case 'local': break;
  default: 
    console.log('unknown environment %s' , environment ); 
    process.exit(); 
  break;
}


console.log("\nCreating %s version of task %s" , environment , task );
console.log('All matching zip files in the following distribution path will be unlinked: %s' , dist_path);
console.log("\nFound %d matching task(s) in the following path: %s\n", tasks.length , task_basepath);

prompt.start();

var schema = {
  properties: {
    'Yes/No': {
      required: true,
      type: 'string',
      default: 'Yes'
    }
  }
};

prompt.message = 'Are you sure you want to create the distribution using the above paths?';
prompt.get( schema, function (err, result) {

  if(err)
  {  
    console.log( err );
    process.exit();
  }

  if( result['Yes/No'].substr(0,1).toLowerCase() != 'y' )
    process.exit();


  // Setup the temporary path where the tasks will be built and packaged
  var tmp_path = path.join( os.tmpdir() , 'lambda-dist' );
  if( !fs.existsSync( tmp_path ) )
    fs.mkdirSync( tmp_path );

  var complete_count = 0;

  var folderCopyOpts = {
          forceDelete: true,
          excludeHiddenUnix: false,
          preserveFiles: false,
          preserveTimestamps: false,
          inflateSymlinks: false,
      };

  tasks.forEach(function( task_filename ){

    var task_path = path.join( task_basepath , task_filename );
    var task_name = path.basename( task_filename , '.js' );
    var suffix = ( environment == 'dev' ? '-dev' : '' );
    var dest_path = path.join( dist_path , task_name ) + suffix + '.zip';

    console.log( util.format("\nCreating archive %s" , dest_path ) );

    if( fs.existsSync( dest_path ) )
      fs.unlinkSync( dest_path );

    var package_files = [
      { source: task_path , target: path.basename( task_filename ) },
      { source: path.join( __dirname , 'app.js' ) , target: 'app.js' },
      { source: path.join( __dirname , '.env' ) , target: '.env' },
      { source: path.join( __dirname , 'toolbox.js' ) , target: 'toolbox.js' }
    ];

    var envDepPath = path.join( __dirname , 'environment-deps' , environment );
    if( fs.existsSync( envDepPath ))
    {
      fs.readdirSync( envDepPath ).forEach(function(file){
        package_files.push({ source: path.join( envDepPath , file ) , target: file });
      });
    }


    var packageJson = JSON.parse( fs.readFileSync( path.join( __dirname , 'package.json' ) ) );

    var zip_files = [];
    var zip_folders = [];

    
    var task_tmp_path = path.join( tmp_path , task_name );
    if( fs.existsSync( task_tmp_path ) )
      wrench.rmdirSyncRecursive( task_tmp_path , true );
    fs.mkdirSync( task_tmp_path );
    var module_path_dest = path.join( task_tmp_path , 'node_modules' );
    fs.mkdirSync( module_path_dest );


    var archive = archiver.create('zip', {});

    archive.on('error', function(err){
      throw err;
    });

    archive.on('end',function(){
      if( ++complete_count < tasks.length )      
        return;

      console.log(util.format('Removing temporary path %s' , tmp_path ));

      if( fs.existsSync( tmp_path ) )
        wrench.rmdirSyncRecursive( tmp_path , true );        
    });

    archive.pipe( fs.createWriteStream( dest_path ) );


    /* 
     * Copy over the task files and common framework files, and update the task file path
     */
    package_files.forEach(function( package_file ){

      if(fs.statSync( package_file.source ).isDirectory())
      {
        var dest_path = path.join( task_tmp_path , package_file.target );

        wrench.copyDirSyncRecursive( package_file.source , dest_path , folderCopyOpts );
        zip_folders.push({ source: dest_path , target: package_file.target });
        return;
      }

      if(fs.existsSync( package_file.source ))
      {
        var source = package_file.source;
        var dest_path = path.join( task_tmp_path , path.basename( package_file.source ) );

        fs.copySync( package_file.source , dest_path );

        // Update the path to the app module in the primary task file
        if( package_file.source == task_path )
        {
          var fileContent = fs.readFileSync( dest_path );
          fileContent = fileContent.toString().replace(/var app = require\(.+\);/i,"var app = require('./app');");
          fs.writeFileSync( dest_path , fileContent );
        }
      
        package_file.source = dest_path;
        zip_files.push( package_file );
      }
    });


    /* 
     * Copy over module dependencies
     */
    var module_deps = [];
    var module_path_source = path.join( __dirname , 'node_modules' );

    var module_sources = fs.readdirSync( module_path_source );

    module_sources.forEach(function( module_name ){

      if( packageJson.exclude_deps.indexOf( module_name ) != -1 )
        return;

      var dest_path = path.join( module_path_dest , module_name );

      wrench.copyDirSyncRecursive( path.join( module_path_source , module_name ) , dest_path , folderCopyOpts );

      module_deps.push({ source: dest_path , target: 'node_modules/' + module_name });
    });  


    /*
     * Write all the files/folders in the prepared temporary path to the zip folder
     */

    module_deps.forEach(function( folder ){
      console.log( util.format(' + %s' , folder.source ) );
      archive.directory( folder.source , folder.target );
    });

    zip_folders.forEach(function( folder ){
      console.log( util.format(' + %s' , folder.source ) );
      archive.directory( folder.source , folder.target );
    });

    zip_files.forEach(function( file ){
      console.log( util.format(' + %s' , file.source ));
      archive.file( file.source , { name: file.target } );
    });

    console.log('Finalizing archive');
    archive.finalize();
  });
});
