
if(process.argv.length < 3)
{
  console.log("Usage: node ./test.js [task name] [event data json]");
  process.exit();
}

var path = require('path');
var fs = require('fs');
var util = require('util');

var task_name = process.argv[2];
var task_path = path.join( __dirname , 'tasks' , task_name ) + '.js';

if(!fs.existsSync( task_path ))
{
  console.log('Could not find task: ' + task_path);
  process.exit();
}

var eventData = {};

// command-line event data input
if( process.argv.length > 3 )
{
  var eventData = process.argv[ 3 ];

  try {
    eventData = JSON.parse( eventData );
  } catch( e ){
    console.log('Error parsing event data: %s' , e);
    process.exit();
  }
}

var context = 
{
  done: function(){
    console.log('...context.done();');
    process.exit();
  },
  fail: function( e ){
    console.error( 'context.fail' );
    console.error( e );
  },
  succeed: function( e ){
    console.log( 'context.succeed' );
    console.log( e );
  }  
};


var task = require( task_path );

task.handler( eventData , context );
